FROM apache/airflow:2.7.1

WORKDIR /opt/airflow/

USER root

RUN apt update && apt install -y procps default-jre

USER airflow

COPY requirements.txt ./
RUN pip install -r requirements.txt

COPY dags ./
COPY spark ./