# DevOps Course Labs

## Описание

DAG состоит из одной задачи, запускающейся на сервере Apache Spark. Внутри нее исполняется простой код для подсчета суммы степеней двоек на распределенных данных.

## Запуск

Чтобы развернуть сервис необходимо установить docker, клонировать репозиторий и выполнить следующую команду из корня:

```
docker compose up -d
```

## Мониторинг

Примеры из Grafana

![](/assets/healthy.png)

![](/assets/unhealthy.png)
