from airflow import DAG
from airflow.decorators import task
from airflow.utils.dates import days_ago
from airflow.providers.apache.spark.operators.spark_submit import SparkSubmitOperator 

with DAG(
    dag_id="spark_job_dag",
    schedule_interval="@daily",
    start_date=days_ago(0),
):
    SparkSubmitOperator(
        task_id="spark_job_task",
        application="/opt/airflow/spark/job.py",
        conn_id="spark_local"
    )