from pyspark import SparkConf, SparkContext
from pyspark.sql import SparkSession

conf = SparkConf().setAppName("PySpark App").setMaster("spark://spark-master:7077")
sc = SparkContext(conf=conf)
spark = SparkSession.builder.config(conf=conf).getOrCreate()

rdd = sc.parallelize(range(1, 100)).map(lambda x: 2 ** x)
total_sum = rdd.sum()

print(f'Sum: {total_sum}')

spark.stop()